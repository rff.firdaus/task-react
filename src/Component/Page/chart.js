import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class LineChart extends Component {
	render() {
		const options = {
			theme: "dark2",
			axisX: {
				type: 'category'
			},
			axisY: {
				type: 'value'

			},
			data: [{
				type: "spline",
				dataPoints: [
					{ x: new Date(2018, 0), y: 0 },
					{ x: new Date(2018, 1), y: 900 },
					{ x: new Date(2018, 11), y: 1800 }
				]
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default LineChart;