import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class ColumChart extends Component {
	constructor() {
		super();
		this.toggleDataSeries = this.toggleDataSeries.bind(this);
	}
	toggleDataSeries(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		this.chart.render();
	}
	render() {
		const options = {
            theme: "dark2",
			title:{
				text: "SENTIMENT BY ONLINE MEDIA"
			},
			legend: {
				verticalAlign: "center",
				horizontalAlign: "right",
				reversed: true,
				cursor: "pointer",
					fontSize: 16,
					itemclick: this.toggleDataSeries
			},
			toolTip: {
				shared: true
			},
			data: [
			{
				type: "stackedColumn100",
				name: "Positif",
                showInLegend: true,
                indexLabel: "{y}%",
				color: "rgba(69, 167, 53, 0.85)",
				dataPoints: [
					{ label: "Wartaekonomi", y:20},
					{ label: "liputan6"},
					{ label: "sindonews"},
					{ label: "tempo"},
					{ label: "okezone"},
					{ label: "tribunews"},
					{ label: "Islamic"},
					{ label: "other",}				]
			},
			{
				type: "stackedColumn100",
				name: "Negatif",
                showInLegend: true,
                indexLabel: "{y}%",
				color: "rgba(174, 7, 33, 0.85)",
				dataPoints: [
					{ label: "Wartaekonomi"},
					{ label: "liputan6"},
					{ label: "sindonews",},
					{ label: "tempo",},
					{ label: "okezone", y: 25},
					{ label: "tribunews"},
					{ label: "Islamic"},
					{ label: "other",	y: 12}
				]
			},
			{
				type: "stackedColumn100",
				name: "Netral",
                showInLegend: true,
                indexLabel: "{y}%",
				color: "rgba(0, 156, 220, 0.85)",
				dataPoints: [
					{ label: "Wartaekonomi", y: 80},
					{ label: "liputan6", y: 100},
					{ label: "sindonews", y: 100},
					{ label: "tempo", y: 100},
					{ label: "okezone",	y: 75},
					{ label: "tribunews", y: 100},
					{ label: "Islamic", y: 100},
					{ label: "other", y: 88}
				]
			}
			]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				 onRef={ref => this.chart = ref}
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default ColumChart;