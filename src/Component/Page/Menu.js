import React from "react";
import { Table, Nav } from "react-bootstrap";
import { ProgressBar } from "react-bootstrap";
import user from '../../img/user.png';
import youtube from '../../img/youtube.png';
import instagram from '../../img/instagram.png';
import facebook from '../../img/facebook.png';
import twitter from '../../img/twitter.png';

class Menu extends React.Component {
    render() {
        return (
            <div className="text-center">
                <div className="col-12 m1-top">
                    <div className="col-12 text-center blue bold m0-bottom">Tokoh</div>
                    <Table bordered variant="dark">
                        <thead>
                            <tr className="text-center grey">
                                <th className="th1">Tokoh</th>
                                <th>Total Talk</th>
                                <th className="th-30">Talk By Social Media</th>
                                <th>Sentiment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="text-center grey-light">
                                <td>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">1.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Ahmad Syafi Maarif</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">2.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Din Syamsuddin</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">3.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Anwar Abbas</Nav.Link>
                                    </div>
                                </td>
                                <td>
                                    <div className="m1-bottom">
                                        <div>2571</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>1398</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>939</div>
                                    </div>
                                </td>
                                <td>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>2751</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className="gabungan">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div className="m1-2-bottom">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={10} key={1} />
                                        <ProgressBar variant="danger" now={33.1} key={2} label={`${33.1}%`} />
                                        <ProgressBar variant="info" now={60.7} key={3} label={`${60.7}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div>
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={5} key={1} />
                                        <ProgressBar variant="danger" now={35.0} key={2} label={`${35.0}%`} />
                                        <ProgressBar variant="info" now={64.3} key={3} label={`${64.3}%`} />
                                        </ProgressBar>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
                <div className="col-12 m1-top">
                    <div className="col-12 text-center blue bold m0-bottom">Organisasi</div>
                    <Table bordered variant="dark">
                        <thead>
                            <tr className="text-center grey">
                                <th className="th1">Organisasi</th>
                                <th>Total Talk</th>
                                <th className="th-30">Talk By Social Media</th>
                                <th>Sentiment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="text-center grey-light">
                                <td>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">1.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Muhammadiyah</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">2.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Aisyiyah</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">3.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Suara Muhammadiyah</Nav.Link>
                                    </div>
                                </td>
                                <td>
                                    <div className="m1-bottom">
                                        <div>2571</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>1398</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>939</div>
                                    </div>
                                </td>
                                <td>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>2751</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className="gabungan">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div className="m1-2-bottom">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={10} key={1} />
                                        <ProgressBar variant="danger" now={33.1} key={2} label={`${33.1}%`} />
                                        <ProgressBar variant="info" now={60.7} key={3} label={`${60.7}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div>
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={5} key={1} />
                                        <ProgressBar variant="danger" now={35.0} key={2} label={`${35.0}%`} />
                                        <ProgressBar variant="info" now={64.3} key={3} label={`${64.3}%`} />
                                        </ProgressBar>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
                <div className="col-12 m1-top">
                    <div className="col-12 text-center blue bold m0-bottom">Amal Usaha</div>
                    <Table bordered variant="dark">
                        <thead>
                            <tr className="text-center grey">
                                <th className="th1">Amal Usaha</th>
                                <th>Total Talk</th>
                                <th className="th-30">Talk By Social Media</th>
                                <th>Sentiment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="text-center grey-light">
                                <td>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">1.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">SD/MI Muhammadiyah</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">2.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">Universitas Muhammadiyah</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">3.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment">SMA/MA/SMK Muhammadiyah</Nav.Link>
                                    </div>
                                </td>
                                <td>
                                    <div className="m1-bottom">
                                        <div>2571</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>1398</div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div>939</div>
                                    </div>
                                </td>
                                <td>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>2751</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row m0-bottom">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={facebook} alt="facebook"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={instagram} alt="instagram"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                        <div className="col-3">
                                            <div className="d-flex align-items-center">
                                                <img className="img m1-right" src={youtube} alt="youtube"></img>
                                                <div>3357</div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className="gabungan">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div className="m1-2-bottom">
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={10} key={1} />
                                        <ProgressBar variant="danger" now={33.1} key={2} label={`${33.1}%`} />
                                        <ProgressBar variant="info" now={60.7} key={3} label={`${60.7}%`} />
                                        </ProgressBar>
                                    </div>
                                    <div>
                                        <ProgressBar>
                                        <ProgressBar variant="success" now={5} key={1} />
                                        <ProgressBar variant="danger" now={35.0} key={2} label={`${35.0}%`} />
                                        <ProgressBar variant="info" now={64.3} key={3} label={`${64.3}%`} />
                                        </ProgressBar>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </div>

        );
    }
}

export default Menu;