import React from "react";
import { Table, Nav } from "react-bootstrap";
import { ProgressBar } from "react-bootstrap";
import user from '../../img/user.png';
import youtube from '../../img/youtube.png';
import instagram from '../../img/instagram.png';
import facebook from '../../img/facebook.png';
import twitter from '../../img/twitter.png';
import Chart from './chart';
import Pie from './pie';
import Colum from './columchart';

class Sentiment extends React.Component {
    render() {
        return (
            <div className="text-center">
                <div className="col-12 m1-top">
                    <Table bordered variant="dark">
                        <thead>
                            <tr className="text-center blue bold">
                                <th className="th">
                                    <div className="d-flex">
                                        <div className="m3-right">No.</div>
                                        <div>Tokoh</div>
                                    </div>
                                </th>
                                <th className="th-16">
                                    <div className="d-flex">
                                        <div className="m2-right">Object</div>
                                        <div className="m2-right">Talk</div>
                                        <div className="m2-right">Engagement</div>
                                        <div className="m2-right">Reach</div>
                                        <div>Sentiment</div>
                                    </div>
                                </th>
                                <th className="th-16">
                                    <div className="d-flex">
                                        <div className="m2-right">Online Media</div>
                                        <div>Sentiment</div>
                                    </div>
                                </th>
                                <th>Day To Day Talk</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="text-center grey-light">
                                <td>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">1.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Ahmad Syafi Maarif</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">2.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Din Syamsuddin</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">3.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Anwar Abbas</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">4.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Haedar Nasir</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">5.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Muhadjir Effendy</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">6.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Abdul Mu'ti</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">7.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Suyatno</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">8.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Sunanto</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">9.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Hajriyanto</Nav.Link>
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <span className="m1-right">10.</span>
                                        <img src={user} className="img2" alt="user"></img>
                                        <Nav.Link href="/sentiment" className="short-text1">Diyah Puspitarini</Nav.Link>
                                    </div>
                                </td>
                                <td>
                                    <div className="m1-bottom">
                                        <div className="d-flex">
                                            <div className="">
                                                <div className="d-flex align-items-center">
                                                    <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                    <div className="short-text">Muhammadiyah</div>
                                                </div>
                                            </div>
                                            <div className="col-2">2.7K</div>
                                            <div className="col-2">11.5K</div>
                                            <div className="col-2">3.4M</div>
                                            <div className="col-4 gabungan">
                                                <ProgressBar>
                                                    <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                                    <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                                    <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                                </ProgressBar>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="m1-bottom">
                                        <div className="d-flex">
                                            <div className="">
                                                <div className="d-flex align-items-center">
                                                    <img className="img m1-right" src={twitter} alt="twitter"></img>
                                                    <div className="short-text">Buya Syafii</div>
                                                </div>
                                            </div>
                                            <div className="col-2">6</div>
                                            <div className="col-2">6</div>
                                            <div className="col-2">53.3k</div>
                                            <div className="col-4 gabungan">
                                                <ProgressBar>
                                                    <ProgressBar variant="success" now={0} key={1} label={`${0}%`} />
                                                    <ProgressBar variant="danger" now={0} key={2} label={`${0}%`} />
                                                    <ProgressBar variant="info" now={100} key={3} label={`${100}%`} />
                                                </ProgressBar>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className="d-flex">
                                        <div className="col-5">1</div>
                                        <div className="col-8 gabungan">
                                            <ProgressBar>
                                                <ProgressBar variant="success" now={0} key={1} label={`${0}%`} />
                                                <ProgressBar variant="danger" now={0} key={2} label={`${0}%`} />
                                                <ProgressBar variant="info" now={100} key={3} label={`${100}%`} />
                                            </ProgressBar>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <Chart />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
                <div className="d-flex">
                    <div className="col-4">
                        <div className="bold grey-light engagement m1-bottom">
                            <h5>ENGAGEMENT BY SOCIAL MEDIA</h5>
                            <div className="gabungan col-12">
                                <ProgressBar>
                                    <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                    <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                    <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                </ProgressBar>
                            </div>
                        </div>
                        <div className="bold grey-light engagement">
                            <h5>SENTIMENT BY SOCIAL MEDIA</h5>
                            <div className="d-flex align-items-center m1-2-bottom">
                                <img className="img m1-right" src={youtube} alt="twitter"></img>
                                <div className="col-10">
                                    <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                    </ProgressBar>
                                </div>
                            </div>
                            <div className="d-flex align-items-center m1-2-bottom">
                                <img className="img m1-right" src={twitter} alt="twitter"></img>
                                <div className="col-10">
                                    <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                    </ProgressBar>
                                </div>
                            </div>
                            <div className="d-flex align-items-center m1-2-bottom">
                                <img className="img m1-right" src={instagram} alt="twitter"></img>
                                <div className="col-10">
                                    <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                    </ProgressBar>
                                </div>
                            </div>
                            <div className="d-flex align-items-center m1-2-bottom">
                                <img className="img m1-right" src={facebook} alt="twitter"></img>
                                <div className="col-10">
                                    <ProgressBar>
                                        <ProgressBar variant="success" now={13} key={1} label={`${13}%`} />
                                        <ProgressBar variant="danger" now={29.6} key={2} label={`${29.6}%`} />
                                        <ProgressBar variant="info" now={57.4} key={3} label={`${57.4}%`} />
                                    </ProgressBar>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pie col-2">
                        <Pie />
                    </div>
                    <div className="col-3 colum">
                        <Colum />
                    </div>
                    <div className="col-3">
                        <div className="engagement bold grey-light" style={{height: '384px'}}>
                            <h5>WORD CLOUD</h5>
                            <p className="h5 d-flex" style={{color: 'yellow'}}>luhut</p>
                            <p className="h6" style={{color: 'green'}}>sari</p>
                            <div className="d-flex">
                                <p className="h6" style={{color: 'red'}}>kerja giliran menolak</p>
                                <p className="h3">yang</p>
                            </div>
                            <p className="h5" style={{color: 'blue'}}>menikah</p>
                            <p className="h6 d-flex" style={{color: 'grey'}}>pekerja</p>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Sentiment;