import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import App from "./Component/Home/App"
import Menu from "./Component/Page/Menu";
import Sentiment from "./Component/Page/sentiment"

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Navbar bg="primary" variant="dark">
                <Navbar.Brand href="/">Muhammadiyah</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="/Menu">OVERVIEW</Nav.Link>
                    <Nav.Link href="/Sentiment">TOKOH</Nav.Link>
                    <Nav.Link href="/">ORGNISASI</Nav.Link>
                    <Nav.Link href="/">AMAL USAHA</Nav.Link>
                </Nav>
            </Navbar>
            <Route path="/" exact component={App} />
            <Route path="/Menu" exact component={Menu} />
            <Route path="/Sentiment" exact component={Sentiment} />
        </div>
    </BrowserRouter>
);


export default AppRouter;